PRM = params_tst_EVE_4CORES1;

function tst_params = params_tst_EVE_4CORES1()
    
    radar_num = 2;
    
    tst_params.q = 16; % 
    tst_params.q_factor = log2(tst_params.q);
    
    smp_num = 256;    % 
    pls_num = 32;     % 
    ant_num = 64;     %
    
    smp_pad = 256;    %
    pls_pad = 128;    % 
    azm_pad = 128;    %
    tst_params.smp_pad = smp_pad;
    
    tx_azm = 4; % number of active tx per pulse for azimuth
%     tx_elv = 4; % number of active tx per pulse for elevation
    tx_num = 8; % number of active tx
    tst_params.tx_num = tx_num;
    
    tst_params.chunk_num = bitshift(smp_num, - tst_params.q_factor);
    tst_params.cube_length_azm = 2*pls_num*smp_num*(4*tx_azm);   % samples
    tst_params.cube_size_azm   = 2*pls_num*smp_num*2*(4*tx_azm); % bytes
    tst_params.cube_length = 2*pls_num*smp_num*(4*tx_num);       % samples

    beam_num = 141;
    

    tst_params.win_scale_factor = 11;
    
    % ant_azm - number of antennas in the azimuth array
    tst_params.ant_azm = 64;
    tst_params.one_chip_azm = 4*tx_azm;
    tst_params.one_chip_ant = 4*tx_num;
    tst_params.one_chip_cube_length = 4*tx_azm*smp_num*pls_num;  % complex samples
    tst_params.hamm_win_A = zeros(azm_pad, 1, 'int16');
    tst_params.hamm_win_A(1:tst_params.ant_azm) = hamming(tst_params.ant_azm)*(2^tst_params.win_scale_factor);
    
    tst_params.pls_pad = pls_pad;
    tst_params.hamm_win_V = zeros(pls_pad, 1, 'int16');
    tst_params.hamm_win_V(1:pls_num) = hamming(pls_num)*(2^tst_params.win_scale_factor);

    tst_params.rng_zpad = 0;
    tst_params.dop_zpad = 0;

    targets = [20.3  50.2  80.1;          % range idxs
               10.3  35.2  100.9;         % doppler idxs
               80.1  40.2  20.2;          % azimuth idxs
               20    20    20 ];          % magnitudes

    % radar data cube creation
    Z = data_create(smp_num, pls_num, ant_num, tx_azm, azm_pad, targets);
    
%    Zn = complex(zeros(size(Z)));
%     Zn(1, :, 1) = Zn(1, :, 1) + (32+1j*0);
%     Zn = complex(ones(size(Z)))*1000;
%    Zn(1, :, :) = 3000 + 1j*0; 
%    Zn = reshape(Zn, [smp_num, pls_num*ant_num]);
    
    % reshape to 2D for simulink imoprt
    %Z = reshape(Z, [smp_num, pls_num*ant_num]);
    Z = reshape(Z, [smp_num*pls_num*ant_num, 1]);
    
%     cube_512x8x64 = load('cube_512x8x64.mat');
%     cube = load("velocities_520.mat");
%     cube = load("D:\Python\dora_512x8x64.mat");
%     S = int16(cube.cubes);
%     S = reshape(S, [smp_num, pls_num*ant_num]);
%     tst_params.S = S;
    %tst_params.part = zeros(4,ant_num/4);
    for i=0:3
        tst_params.ant_part{i+1} = [i*ant_num/4 + 1:(i+1)*ant_num/4];
        tst_params.smp_part{i+1} = [i*smp_num/4 + 1:(i+1)*smp_num/4];
        tst_params.pls_part{i+1} = [i*pls_num/4 + 1:(i+1)*pls_num/4];
    end
    
    row = [0:ant_num*pls_num-1];
    pitch = smp_num*2;
    
    smp_part_len = 2*smp_num/4;
    
    smp_vec = zeros(smp_part_len*ant_num*pls_num, 1);
    for i = 1:4
        part_offset = (i-1)*smp_part_len;
        for row_num = [0:ant_num*pls_num-1]
            offset = row_num*pitch;
            row_offset = row_num*smp_part_len;
            smp_part = [offset + part_offset + 1:offset + part_offset + smp_part_len];
            smp_vec(row_offset+1:row_offset+smp_part_len) = smp_part;
        end
        tst_params.smp_vec_part{i} = smp_vec;  
    end
    
    
    pls_part = 2*smp_num*ant_num*pls_num/4; 
    for i = 0:3
        tst_params.pls_part_vec{i+1} = [i*pls_part+1:(i+1)*pls_part];
    end
    
    % CFAR parameters
    nGuardRng = 2;
    nTrainRng = 4;
    nGuardDop = 2;
    nTrainDop = 4;

    tst_params.guard_size = [nGuardRng nGuardDop];
    tst_params.train_size = [nTrainRng nTrainDop];
    
    tst_params.max_targets = 512;
    
    sector = [-70 70]; % in degrees
    sector_width = sector(2) - sector(1); % in degrees
    [W, azm] = generate_bfm_weights(ant_num, azm_pad, beam_num, sector,  'bfm');
    tst_params.W = W;
    tst_params.Wm.Re = reshape(W.Re, [ant_num, beam_num]);
    tst_params.Wm.Im = reshape(W.Im, [ant_num, beam_num]);
    tst_params.azm = azm;
%     tst_params.W, tst_params.azm = generate_bfm_weights(ant_num, beam_num);
    
    %% Elevation beamformer parameters 
    elv_beam_num = 81;
    tst_params.elv_beam_num = elv_beam_num;
    elv_sector = [-20 20];
    elv_pad = 128;
    elv_ant_num = 5;
    

    %% Load calibration vectors
    tst_params.clbe_dummy = complex(ones(5, 1,'single'), zeros(5, 1,'single'));
    tst_params.clba_dummy = complex(ones(64, 1,'single'), zeros(64, 1,'single'));
    
    clb_dict = load("clb_dict.mat");
    tst_params.clbe1 = single(clb_dict.clbe_1.');       % 5 elements
    tst_params.clba1 = single(clb_dict.clba_1.');
    
    tst_params.clbe2 = single(clb_dict.clbe_2.');       % 5 elements
    tst_params.clba2 = single(clb_dict.clba_2.');
    
    if (radar_num == 1)
        if (elv_ant_num == 4)
            tst_params.clbe = tst_params.clbe1(2:5);
        else
            tst_params.clbe = tst_params.clbe1;
        end
        tst_params.clba = tst_params.clba1;
        tst_params.udp_tx_port = 25205;
        tst_params.udp_rx_port = 35205;
    elseif (radar_num == 2)
        if (elv_ant_num == 4)
            tst_params.clbe = tst_params.clbe2(2:5);
        else
            tst_params.clbe = tst_params.clbe2;
        end
        tst_params.clba = tst_params.clba2;
        tst_params.udp_tx_port = 25200;
        tst_params.udp_rx_port = 35200;
    end
        
    %%
    [We, elv] = generate_bfm_weights(elv_ant_num, elv_pad, elv_beam_num, elv_sector,  'bfm');
    tst_params.We = We;
    tst_params.Wm_elv.Re = reshape(We.Re, [elv_ant_num, elv_beam_num]);
    tst_params.Wm_elv.Im = reshape(We.Im, [elv_ant_num, elv_beam_num]);
    tst_params.Wm_elv_complex = complex(tst_params.Wm_elv.Re, tst_params.Wm_elv.Im);


    
    tst_params.smp_num = smp_num;
    tst_params.pls_num = pls_num;
    tst_params.ant_num = ant_num;
    tst_params.beam_num = beam_num;
    tst_params.azm_pad = azm_pad;
    tst_params.Z = Z;
    %tst_params.Zn = int16(Zn);

    %% Grids generation
    startfreq = 77e9;
    fs = 18.75e6;
    S = 90e12;
    rampendtime = 22e-6;
    idletime = 8e-6;
    tp = rampendtime + idletime;

    rmax = 3e8*fs/(2*S);
    rres = rmax/smp_num;
    
    lmb = 3e8/startfreq;
    vmax = lmb / (4*tx_azm*tp); 
    vres = 2*vmax/pls_num;
    tst_params.cvmax = ceil(vmax);
    
    % hardcode version for "velocities_520.mat"
%     rmax = 112.5;
%     rres = 0.2197265625*2;
%     vmax = 9.276437847866418;
%     vres = 0.1449443413729128;
%     beam_num = 128;

    tst_params.rng_grid = single([0:rres:rmax - rres].');
    tst_params.azm_grid = single(azm.');
    tst_params.vel_grid = single(fftshift([-vmax:vres:vmax - vres]).');
    tst_params.elv_grid = single(elv.');
    
    tst_params.azm_grid_fft = single(fftshift(asin(2/azm_pad*([0:azm_pad - 1] - azm_pad/2))).');
    
    %%
    human_factor = 26000000;
    a = 2.4.^2 * human_factor;
    tst_params.thr = int32(a./(tst_params.rng_grid.^2));
    
    %%
    human_factor_R = 100000000; %250000000
    a = 2.2^2 * human_factor_R;
    tst_params.thrR = single(a./(tst_params.rng_grid.^2));
    tst_params.human_factor_R = human_factor_R;
    %%
    indexx = zeros(ant_num*2, 1, 'uint16');
    for i = 1:ant_num
        ii = (i - 1)*tst_params.q*2;
        indexx(2*i - 1) = ii;
        indexx(2*i) = ii + 1;
    end
    tst_params.reim_idx = indexx;
    
   tvec = ones(128,1,'int16');
   for i = 1:tst_params.q
        ii = i - 1;
        tvec(i:tst_params.q:end) = 2.^ii;
   end
   tst_params.tvec = tvec;
   
   awinscale = 16;
   awin = int32(hamming(ant_num).*(2.^awinscale)).';
   tst_params.reim_awin = reshape(cat(1, awin, awin), [2*ant_num 1]);
   
%% calibration 
%    %% Doppler compensation coefficients
% %    phidop = 2*pi.*[0:pls_num-1]./pls_num;
%    phidop = 2*pi.*([0:pls_num-1]-pls_num/2)./pls_num;
%    
% %    dopcorr_vector = (phidop * (1/2));
% %    mimo_vector = [0 0 0 0 1 1 1 1].';
%    
%    mimo_vector = zeros(4,1);
%    for k = 1:tx_azm-1
%        mimo_vector = cat(1, mimo_vector, k*ones(4,1)/tx_azm);
%    end
%    dopcorr_matrix = mimo_vector*phidop;
%    
% %    dopcorr_matrix = mimo_vector * dopcorr_vector;
%    dopcorr_matrix = fftshift(exp(-1j.*dopcorr_matrix), 2);
%    
%    
%    tst_params.dm_double = dopcorr_matrix;
%    
%    dopcorr_matrix_win = zeros(ant_num, pls_num);
%    for i = 1:pls_num 
%         dopcorr_matrix_win(:,i) = dopcorr_matrix(:,i) .* hamming(ant_num);
%    end
%    
%    dopcorr_scale_factor = 12;
%    tst_params.dopcorr_scale_factor = uint16(dopcorr_scale_factor);
%    dopcorr_matrix = int16(dopcorr_matrix .* 2^dopcorr_scale_factor);
%    dopcorr_matrix_win3 = int16(dopcorr_matrix_win .* 2^dopcorr_scale_factor);
%    
% %    dopcorr_matrix_win = conj(dopcorr_matrix_win);
%    dopcorr_matrix_win = int32(dopcorr_matrix_win .* 2^31);
%    
%    % doppler indexes 2:3 and 127:128 have no correction
%    blind_dop = 2;
%    for i = 1:blind_dop
%         dopcorr_matrix_win(:,1+i) = dopcorr_matrix_win(:,1);
%         dopcorr_matrix_win(:,pls_num-i+1) = dopcorr_matrix_win(:,1);
%         
%         dopcorr_matrix_win3(:,1+i) = dopcorr_matrix_win3(:,1);
%         dopcorr_matrix_win3(:,pls_num-i+1) = dopcorr_matrix_win3(:,1);
%         
%         dopcorr_matrix(:,1+i) = dopcorr_matrix(:,1);
%         dopcorr_matrix(:,pls_num-i+1) = dopcorr_matrix(:,1);
%    end
%       
%    dopcorr_matrix_win2 = zeros(ant_num*2, pls_num, 'int32');
%    dopcorr_matrix_win2(1:2:end,:) = real(dopcorr_matrix_win);
%    dopcorr_matrix_win2(2:2:end,:) = imag(dopcorr_matrix_win);
%    tst_params.dopcorr_matrix_win = dopcorr_matrix_win2;
%    
%    dopcorr_matrix_win4 = zeros(ant_num*2, pls_num, 'int16');
%    dopcorr_matrix_win4(1:2:end,:) = real(dopcorr_matrix_win3);
%    dopcorr_matrix_win4(2:2:end,:) = imag(dopcorr_matrix_win3);
%    
%    
%    dopcorr_matrix_intr = zeros(ant_num*2, pls_num, 'int16');
%    dopcorr_matrix_intr(1:2:end,:) = real(dopcorr_matrix);
%    dopcorr_matrix_intr(2:2:end,:) = imag(dopcorr_matrix);
%    tst_params.dopcorr_matrix = int32(dopcorr_matrix_intr);
%    
%    dopcorr_matrix_intr = reshape(dopcorr_matrix_intr,[],1);
%    tst_params.dopcorr = dopcorr_matrix_intr;
%    dopcorr_matrix_win4 = reshape(dopcorr_matrix_win4,[],1);
%    tst_params.dopcorr_win = dopcorr_matrix_win4;
%    
end


function z = data_create(smp_num, pls_num, ant_num, tx_azm, ant_pad, targets)
    
    z = complex(zeros(pls_num, smp_num, ant_num), 0);
    for i = 1:size(targets, 2)
        rng_peak_idx = targets(1, i);
        dop_peak_idx = targets(2, i);
        azm_peak_idx = targets(3, i);
        magnitude = targets(4, i);
        
        % real part
        % 2*smp_num for oversampling elimination
        x = cos(2*pi*rng_peak_idx/(2*smp_num)*[0:2*smp_num-1]);
        % complex envelope
        y = hilbert(x);
        % decimation by factor of 2
        y = y(1:2:2*smp_num);

        % template for pls_num pulses
        y = ones(pls_num, 1) * y;
        
        % doppler phase shifts for dop_peak_idx
        dop_phase_shifts = exp(-1j*2*pi*dop_peak_idx/pls_num*[0:pls_num-1]);
               
        % doppler shift creation over pulses
        y = (y' .* dop_phase_shifts)';
        
        % mimo Doppler shift creation
        if dop_peak_idx < pls_num/2
            phidop = 2*pi*dop_peak_idx/pls_num;
        else
            phidop = 2*pi*(dop_peak_idx - pls_num/2)/pls_num - pi;
        end

%         mimo_vector = [0 0 0 0];
%         for k = 1:tx_azm-1
%             mimo_vector = cat(2, mimo_vector, k*ones(1,4)/tx_azm);
%         end
%         mimo_vector = mimo_vector*phidop;
%         mimo_phase_shift = exp(1j*mimo_vector);
        
        % antenna's phase shifts for azm_peak_idx
        azm_phase_shifts = exp(1j*2*pi*azm_peak_idx/ant_pad*[0:ant_num-1]);
        
        % add extra mimo Doppler shift
%         azm_phase_shifts = azm_phase_shifts.*mimo_phase_shift;
        
        % add antenna phase shifts
        for j = 1:ant_num
            z(:, :, j) = z(:, :, j) + y * magnitude * azm_phase_shifts(j);
        end
        
    end
    
%     rvmap = mean(abs(fft2(z)), 3);
%     imshow(rvmap)
    z = permute(int16(z), [2 3 1]);
    
end


function [W2, azm] = generate_bfm_weights(ant_num, azm_pad, beam_num, sector, bfm_style)

    if (bfm_style == 'fft')
        % fft style azims
        azm = asin([-1:1/(azm_pad/2):1-1/(azm_pad/2)]);
        azm = fftshift(azm);
        % beamformer weights
        W = complex(zeros(azm_pad, ant_num, 'int16'), 0);
        for i = 1:ant_num
            W(:, i) = 2^13*exp(-1j*2*pi*[0:(azm_pad - 1)]/azm_pad*(i - 1));
        end
        W2.Re = reshape(real(W).', [azm_pad*ant_num 1]);
        W2.Im = reshape(imag(W).', [azm_pad*ant_num 1]);
    else
        % uniform style beamforming azims
        sector_width = sector(2) - sector(1);
        st = sector_width/(beam_num - 1);
        azm = deg2rad([sector(1):st:sector(2)]);
%         azm = [-pi/2:pi/beam_num:pi/2-pi/beam_num];
        W = complex(zeros(beam_num, ant_num, 'int16'), 0);
        for i = 1:ant_num
            W(:, i) = 2^13*exp(-1j*pi*sin(azm)*i);
        end
%         W(:, 5:8) = 0; % 4RX only 
        W2.Re = reshape(real(W).', [beam_num*ant_num 1]);
        W2.Im = reshape(imag(W).', [beam_num*ant_num 1]);
    end
%     for i = 1:ant_num
%         if (bfm_style == 'fft')
%             % fft style
% %             W(:, i) = 2^15*exp(-1j*2*pi*[1:beam_num]/beam_num*(i - 1));
%             W(:, i) = 2^13*exp(-1j*2*pi*[0:(beam_num - 1)]/beam_num*(i - 1));
%         else
% 
%             % bfm style
%             W(:, i) = 2^13*exp(-1j*pi*sin(azm)*i);
%         end
%     end
% 
%     W2.Re = reshape(real(W).', [beam_num*ant_num 1]);
%     W2.Im = reshape(imag(W).', [beam_num*ant_num 1]);
end